import React, { HTMLAttributes } from 'react';

export const MessageGenericPreviewThumb = (
  props: HTMLAttributes<HTMLDivElement>
) => <div className='rcx-message-generic-preview__thumb' {...props} />;
